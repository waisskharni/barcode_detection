/*
 * Copyright (c) 2019, e-con Systems India Pvt. Ltd CORPORATION. All rights reserved.
 * Author : Waiss Kharni <waisskharni.sm@e-consystems.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of NVIDIA CORPORATION nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "main.h"

using namespace cv;
using namespace std;

const char* keys =
{
    "{help h  | | show help message}"
    "{image   | | input Image}"
    "{verbose v | | enable debug messages}"
    "{time t | | display time taken for operation}"
};

void help(char *projectName)
{
    cout << "\nThis project detects the location of a Barcode (1D or 2D)\n"
            "\nUsage: \n" << projectName << " --image=(Abs path of Image)";

    cout << "\nOptions " << endl;
    cout << "--help (or) -h\t\t Display available flags and usage. " << endl;
    cout << "--verbose (or) -v\t Print debug messages. " << endl;
    cout << "--time (or) -t\t Generate Time Report. " << endl;
}

int main(int argc, char **argv)
{
    char *projectName = argv[0];
    Mat Image, srcGrayImage;
    bool esc_key = false;
    int key;

    /* Command Line Parser for terminal */
    CommandLineParser parser(argc,argv,keys);
    if (parser.has("help") || argc < 2)
    {
	help(projectName);
	return 0;
    }

    String imPath = parser.get<String>("image");
    cout << "Looking for Image : " << imPath << endl;
    Image = imread(imPath, IMREAD_COLOR);
    if (Image.empty()) {
	cout << "Could not locate Image" << endl;
	return -1;
    }

    BarcodeDetector barcodeDetector(Image);

    namedWindow("Input Image", WINDOW_AUTOSIZE);
    imshow("Input Image", Image);
    key = waitKey(0);
    if (key == 27)
	esc_key = true;

    if (esc_key)
	return 0;
    else {
	barcodeDetector.processBarcode();
	while(key != 27) { 
	    key = waitKey(0);
	    if (key == 27)
	    	break;
	}
    }

    return 0;
}
