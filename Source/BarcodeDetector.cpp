/*
 * Copyright (c) 2019, e-con Systems India Pvt. Ltd CORPORATION. All rights reserved.
 * Author : Waiss Kharni <waisskharni.sm@e-consystems.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of NVIDIA CORPORATION nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "main.h"

/* Macro Declarations */
#define IMAGE_PROCESS	1

using namespace cv;
using namespace std;

BarcodeDetector::BarcodeDetector(Mat Image)
	: m_inputImg(Image)
	, m_morphChangesDone(false)
	, m_contouringDone(false)
{
    namedWindow( "Output Stream" , CV_WINDOW_AUTOSIZE );
}

/* Processing steps for detecting Barcode
 * 1. Conversion of color Image to Gray scale
 * 2. Finding edges in the grayscale image using Laplacian filter
 * 3. Finding Gradient on the Edge Data
 * 4. Converting the Grayscale Gradient Image to Binary
 * 5. 4 Connectivity Hole filling in Binary Image
 */
bool BarcodeDetector::processBarcode()
{
    // Convert Color image to gray
    cvtColor(m_inputImg, m_grayImg, CV_RGB2GRAY);

    // Finding edges using laplacian filter
    int kernel_size = 3;
    int scale = 2;
    int delta = 0;
    int ddepth = CV_16S;

    Laplacian( m_grayImg, m_processBuffer1, ddepth, kernel_size, scale, delta, BORDER_DEFAULT);
    convertScaleAbs( m_processBuffer1, m_processBuffer2 );

#if IMAGE_PROCESS
    DisplayMat(m_processBuffer2);
#endif

    // Adaptive Thresholding based Binarization
    threshold(m_processBuffer2, m_processBuffer1, 225, 255, THRESH_BINARY);

#if IMAGE_PROCESS
    DisplayMat(m_processBuffer1);
#endif

    // Applying Closing Morphological filter on Binary Image
    Mat elem1 = getStructuringElement( MORPH_RECT, Size(7, 5), Point( -1, -1 ) );
    morphologyEx(m_processBuffer1, m_processBuffer2, MORPH_CLOSE, elem1);

#if IMAGE_PROCESS
    DisplayMat(m_processBuffer2);
#endif

    morphologyEx(m_processBuffer2, m_processBuffer1, MORPH_CLOSE, elem1);

#if IMAGE_PROCESS
    DisplayMat(m_processBuffer1);
#endif

    morphologyEx(m_processBuffer1, m_processBuffer2, MORPH_OPEN, elem1);

#if IMAGE_PROCESS
    DisplayMat(m_processBuffer2);
#endif

    if (!MorphChanges(m_processBuffer2))
	return false;

    if (!Contours())
	return false;

    return true;
}

void BarcodeDetector::DisplayMat(Mat outputMat, const char *WindowName)
{
    imshow(WindowName, outputMat);
    waitKey(0);
}

bool BarcodeDetector::Contours()
{
    if (!m_morphChangesDone)
	return false;

    vector<vector<Point> > contours;
    findContours(m_processBuffer1, contours, RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(-1, -1));
    vector<Rect> MaskRectangles;
    vector<Mat> croppedImages;
    Mat croppedImage;

    printf("Contour Size : %d\n", contours.size());
    for (uint32_t i  = 0; i < contours.size(); i++) {
	printf("Elements in each contour : %d \n", contours[i].size());
    	int32_t x_min = 640, x_max = 0, y_min = 480, y_max = 0;
	for (uint32_t j = 1; j <= contours[i].size(); j++) {
	    // Find the corners from Contour
	    if (contours[i][j-1].x <= x_min)
		x_min = contours[i][j-1].x;

	    if (contours[i][j-1].x >= x_max)
		x_max = contours[i][j-1].x;

	    if (contours[i][j-1].y <= y_min)
		y_min = contours[i][j-1].y;

	    if (contours[i][j-1].y >= y_max)
		y_max = contours[i][j-1].y;
	}
	int32_t startX = x_min >= 20 ? x_min - 20 : 0;
	int32_t startY = y_min >= 20 ? y_min - 20 : 0;
	int32_t xWidth = (x_min + x_max - startX) <= 640 ? (x_max - startX) + 20 : 639 - startX;
	int32_t yHeight = (y_min + y_max - startY) <= 480 ? (y_max - startY) + 20 : 479 - startY;
	printf("x_max : %u, y_max : %u \n",x_max, y_max);
	printf("x_min : %u, x_width : %u, y_min : %u, y_height : %u \n",startX, xWidth, startY, yHeight);
	if ((x_max - x_min) >= 20 && (y_max - y_min) >= 20)
	    MaskRectangles.push_back(Rect(startX, startY, xWidth, yHeight));
    }

    for (uint32_t i = 0; i < MaskRectangles.size(); i++) {
    	Mat ROI(m_grayImg, MaskRectangles[i]);
    	ROI.copyTo(croppedImage);
	croppedImages.push_back(croppedImage);

	ostringstream windowName;
	windowName << "cropped Image " << i;
    	imshow(windowName.str().c_str(), croppedImage);
	waitKey(0);
    }

    rotateBarcode(croppedImages, MaskRectangles);

    return true;
}

bool BarcodeDetector::rotateBarcode(vector<Mat> barcodeList, vector<Rect> regionList)
{
    for (uint32_t i = 0; i < regionList.size(); i++) {
	float ratio = (float)regionList[i].width / (float) regionList[i].height;
	if (ratio >= 0.8)
	    printf("Barcode in acceptable Range : %f\n",ratio);
	else {
	    printf("Barcode orientation incorrect : %f , Need to rotate\n",ratio);
	    // Logic to Rotate Image in OpenCV
	    Mat rotateMat;
	    Point2f pt(barcodeList[i].cols/2, barcodeList[i].rows/2);
	    Mat r = getRotationMatrix2D(pt, 270, 1.0);
	    warpAffine(barcodeList[i], rotateMat, r, Size(regionList[i].width, regionList[i].height));
	    DisplayMat(rotateMat, "Rotated Barcode");
	}
	 
    }
    return true;
}

bool BarcodeDetector::MorphChanges(Mat inputMat)
{
    Mat elem = getStructuringElement( MORPH_RECT, Size(7, 7), Point( -1, -1 ) );

    // Applying Opening Morphological filter on Binary Image
    morphologyEx(inputMat, m_processBuffer1, MORPH_ERODE, elem);
#if IMAGE_PROCESS
    DisplayMat(m_processBuffer1);
#endif

    // Applying Opening Morphological filter on Binary Image
    morphologyEx(m_processBuffer1, m_processBuffer2, MORPH_ERODE, elem);

#if IMAGE_PROCESS
    DisplayMat(m_processBuffer2);
#endif

    // Applying Dilating Morphological filter on Binary Image
    morphologyEx(m_processBuffer2, m_processBuffer1, MORPH_ERODE, elem);

#if IMAGE_PROCESS
    DisplayMat(m_processBuffer1);
#endif

    // Applying Dilating Morphological filter on Binary Image
    morphologyEx(m_processBuffer1, m_processBuffer2, MORPH_ERODE, elem);

#if IMAGE_PROCESS
    DisplayMat(m_processBuffer2);
#endif

    // Applying Dilating Morphological filter on Binary Image
    morphologyEx(m_processBuffer2, m_processBuffer1, MORPH_ERODE, elem);

#if IMAGE_PROCESS
    DisplayMat(m_processBuffer1);
#endif

    // Applying Dilating Morphological filter on Binary Image
    morphologyEx(m_processBuffer1, m_processBuffer2, MORPH_DILATE, elem);

#if IMAGE_PROCESS
    DisplayMat(m_processBuffer2);
#endif

    morphologyEx(m_processBuffer2, m_processBuffer1, MORPH_DILATE, elem);

#if IMAGE_PROCESS
    DisplayMat(m_processBuffer1);
#endif

    morphologyEx(m_processBuffer1, m_processBuffer2, MORPH_DILATE, elem);

#if IMAGE_PROCESS
    DisplayMat(m_processBuffer2);
#endif

    morphologyEx(m_processBuffer2, m_processBuffer1, MORPH_DILATE, elem);

#if IMAGE_PROCESS
    DisplayMat(m_processBuffer1);
#endif

    morphologyEx(m_processBuffer1, m_processBuffer2, MORPH_DILATE, elem);

#if IMAGE_PROCESS
    DisplayMat(m_processBuffer2);
#endif

    // Mask the source image with processed value
    Mat outputMat(m_grayImg.rows, m_grayImg.cols, m_grayImg.type());
    Mat cropPointMat(m_grayImg.rows, m_grayImg.cols, m_grayImg.type());

    bitwise_and(m_grayImg, m_processBuffer2, outputMat);

#if IMAGE_PROCESS
    DisplayMat(outputMat, "Masked Output");
#endif

    m_morphChangesDone = true;
    return true;
}

BarcodeDetector::~BarcodeDetector()
{
}

