/*
 * Copyright (c) 2019, e-con Systems India Pvt. Ltd CORPORATION. All rights reserved.
 * Author : Waiss Kharni <waisskharni.sm@e-consystems.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of NVIDIA CORPORATION nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef DETECTBARCODE_H
#define DETECTBARCODE_H

#include "main.h"

#define DEFAULT_WINDOW_NAME "Output Stream"

using namespace cv;
using namespace std;

struct timeReport
{
    double laplacian;
    double gradient;
    double gray2bin;
    double closing;
    double opening;
    double connectivity;
};

class BarcodeDetector 
{
public:
    explicit BarcodeDetector(Mat Image);

    ~BarcodeDetector();

    bool processBarcode();
private:
    bool MorphChanges(Mat inputMat);

    bool Contours();

    void DisplayMat(Mat outputMat, const char *WindowName = DEFAULT_WINDOW_NAME);

    bool rotateBarcode(vector<Mat> barcodeList, vector<Rect> regionList);

    Mat m_inputImg;
    bool m_morphChangesDone;
    bool m_contouringDone;
    Mat m_grayImg;
    Mat m_processBuffer1;
    Mat m_processBuffer2;

    // Time Report generator
    struct timeReport BarcodeTimeReport;
};

#endif // DETECTBARCODE_H    
